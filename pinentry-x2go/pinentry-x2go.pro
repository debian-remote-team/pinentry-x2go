######################################################################
# Automatically generated by qmake (2.01a) Fr Aug 3 07:44:35 2007
######################################################################

INSTALLS += target 
target.path = $${PREFIX}/bin
TRANSLATIONS += pinentry-x2go_da.ts \
                pinentry-x2go_de.ts \
                pinentry-x2go_es.ts \
                pinentry-x2go_et.ts \
                pinentry-x2go_fi.ts \
                pinentry-x2go_fr.ts \
                pinentry-x2go_nb_no.ts \
                pinentry-x2go_nl.ts \
                pinentry-x2go_pt.ts \
                pinentry-x2go_ru.ts \
                pinentry-x2go_sv.ts \
                pinentry-x2go_tr.ts \
                pinentry-x2go_zh_tw.ts

TEMPLATE = app
TARGET = pinentry-x2go
DEPENDPATH += .
INCLUDEPATH += .
QT += widgets

# Input
HEADERS += pinentrydialog.h pinlineedit.h
FORMS += templatedlg.ui
SOURCES += main.cpp pinentrydialog.cpp pinlineedit.cpp
INCLUDEPATH += . .. ../pinentry ../assuan
LIBS += ../pinentry/libpinentry.a ../assuan/libassuan.a ../secmem/libsecmem.a
RESOURCES += resources.rcc
