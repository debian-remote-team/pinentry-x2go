<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="tr">
<context>
    <name>PinEntryDialog</name>
    <message>
        <location filename="pinentrydialog.cpp" line="38"/>
        <source>X2Go Pinpad</source>
        <translation>X2Go Pinpad</translation>
    </message>
    <message>
        <location filename="pinentrydialog.cpp" line="166"/>
        <source>Please, enter your PIN</source>
        <translation>Lütfen, PIN kodunuzu girin</translation>
    </message>
</context>
<context>
    <name>TemplateDialog</name>
    <message>
        <location filename="templatedlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Pencere</translation>
    </message>
</context>
</TS>
