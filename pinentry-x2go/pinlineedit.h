//
// C++ Interface: pinlineedit
//
// Description:
//
//
// Author: Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>, (C) 2007-2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PINLINEEDIT_H
#define PINLINEEDIT_H

#include <QLineEdit>

/**
    @author Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
*/
class PinLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    PinLineEdit(QWidget* parent = 0 );
    ~PinLineEdit();

protected:
    virtual void keyPressEvent( QKeyEvent *e );

signals:
    void bs_pressed();
    void cancel_pressed();
};

#endif
