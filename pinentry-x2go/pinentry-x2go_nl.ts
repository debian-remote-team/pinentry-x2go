<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="nl_NL">
<context>
    <name>PinEntryDialog</name>
    <message>
        <location filename="pinentrydialog.cpp" line="38"/>
        <source>X2Go Pinpad</source>
        <translation>X2Go pinpad</translation>
    </message>
    <message>
        <location filename="pinentrydialog.cpp" line="166"/>
        <source>Please, enter your PIN</source>
        <translation>Geef aub uw PIN code in</translation>
    </message>
</context>
<context>
    <name>TemplateDialog</name>
    <message>
        <location filename="templatedlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
</context>
</TS>
