//
// C++ Implementation: pinlineedit
//
// Description:
//
//
// Author: Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>, (C) 2007-2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pinlineedit.h"
#include <QDebug>
#include <QKeyEvent>

PinLineEdit::PinLineEdit(QWidget* parent): QLineEdit (parent)
{}


PinLineEdit::~PinLineEdit()
{}


void PinLineEdit::keyPressEvent( QKeyEvent *e )
{
    //for Cherry keyboard
    switch(e->key())
    {
    case Qt::Key_Minus:
        emit bs_pressed();
        break;
    case Qt::Key_Plus:
        emit cancel_pressed();
        break;
    default:
        QLineEdit::keyPressEvent( e );
    }
}
