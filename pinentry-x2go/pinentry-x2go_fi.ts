<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fi_FI">
<context>
    <name>PinEntryDialog</name>
    <message>
        <location filename="pinentrydialog.cpp" line="38"/>
        <source>X2Go Pinpad</source>
        <translation>X2Go Pinpad</translation>
    </message>
    <message>
        <location filename="pinentrydialog.cpp" line="166"/>
        <source>Please, enter your PIN</source>
        <translation>Tallenna PIN -koodi</translation>
    </message>
</context>
<context>
    <name>TemplateDialog</name>
    <message>
        <location filename="templatedlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialogi</translation>
    </message>
</context>
</TS>
