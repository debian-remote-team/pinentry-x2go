<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>PinEntryDialog</name>
    <message>
        <location filename="pinentrydialog.cpp" line="38"/>
        <source>X2Go Pinpad</source>
        <translation>X2Go PIN-Pad</translation>
    </message>
    <message>
        <location filename="pinentrydialog.cpp" line="166"/>
        <source>Please, enter your PIN</source>
        <translation>Geben Sie bitte Ihre PIN ein</translation>
    </message>
</context>
<context>
    <name>TemplateDialog</name>
    <message>
        <location filename="templatedlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
</context>
</TS>
